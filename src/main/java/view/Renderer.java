package view;

import org.apache.commons.lang3.tuple.Pair;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Renderer {
    
    public static final double SCALE = 32;

    public Renderer() {
        
    }
    
    public Circle renderBall(Pair<Double, Double> position, double radius) {
        return new Circle(position.getLeft() * SCALE, position.getRight() * SCALE, radius * SCALE, Color.BLACK);        
    }
    
    public Rectangle renderObstacle(Pair<Double, Double> position, double width, double height, double angle) {
        Rectangle obstacle = new Rectangle(width * SCALE, height * SCALE, Color.GOLD);
        obstacle.setX((position.getLeft() - width/2) * SCALE);
        obstacle.setY((position.getRight() - height/2) * SCALE);
        obstacle.setRotate(angle);
        return obstacle;
    }

}
