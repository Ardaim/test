package controller;

import java.awt.Toolkit;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import model.Ball;
import model.MyWorld;
import model.Obstacle;
import view.Renderer;

public class Controller {

    private static final double SCENE_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    private static final double SCENE_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
    
    private MyWorld world;
    private Ball ball;
    private Renderer renderer;

    public static final double SCALE = 32;
    
    public Controller() {
        this.world = new MyWorld();
        this.renderer = new Renderer();
    }
    
   public void inizializeWorld() {
       this.ball = new Ball(0.3, Pair.of(0.,SCENE_HEIGHT/(2 * SCALE)));
       this.world.addBall(ball);
        for (int i = 0; i < 50; i ++) {
            this.world.addObstacle(new Obstacle(3, 0.3, getRandomInBound(-30, 30), getRandomPosition()));
        }
//       this.world.addObstacle(new Obstacle(100, 10, 0, Pair.of(0.,-100.)));
    }
       
    public Circle getBall() {
        return this.renderer.renderBall(this.ball.getPosition(), this.ball.getRadius());
    }
    
    public List<Rectangle> getObstacles() {
        List<Obstacle> obstacles = this.world.getObastacles();
        List<Rectangle> renderedObstacles = new LinkedList<>();
        obstacles.forEach(o -> renderedObstacles.add(this.renderer.renderObstacle(o.getPosition(), o.getWidth(),  o.getHeight(),  o.getAngle())));
        return renderedObstacles;
    }
    
    public void update(double elapsedTime) {
        this.world.update(elapsedTime);
    }
    
    //utility methods
 
    private double getRandomInBound(double min, double max) {
        return (Math.random() * ((max - min) + 1)) + min;
    }
    
    private Pair<Double, Double> getRandomPosition() {
        return Pair.of(getRandomInBound(-SCENE_WIDTH/(2 * SCALE), SCENE_WIDTH/(2 *SCALE)), getRandomInBound(-SCENE_HEIGHT/(2 * SCALE), 0));
    }
    
 
}
