package model;

import org.apache.commons.lang3.tuple.Pair;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Vector2;

public class Ball {

    private Body physicalBody;
//    private Pair<Double, Double> position;
    
    public Ball(double radius, Pair<Double, Double> position) {

//        this.position = position;
        
        Circle shape = new Circle(radius);
        shape.translate(shape.getCenter().getNegative());
        shape.translate(position.getLeft(), position.getRight());
        
        BodyFixture fixture = new BodyFixture(shape);
        fixture.setRestitution(0.8);
        
        this.physicalBody = new Body();
        this.physicalBody.addFixture(fixture);
        this.physicalBody.setMass(MassType.NORMAL);
        this.physicalBody.setBullet(true);
        this.physicalBody.setLinearDamping(0.5);
        this.physicalBody.getLinearVelocity().set(0, 0);
        this.physicalBody.setGravityScale(2);

    }
    
    protected void update() {
//        Pair<Double, Double> previousPosition = Pair.of(this.position);
//        Vector2 changeInPosition = this.physicalBody.getChangeInPosition();
//        this.position = Pair.of(previousPosition.getLeft() + changeInPosition.x, previousPosition.getRight() + changeInPosition.y);
    }
    
//    public void move(double x, double y) {
//        this.physicalBody.translate(x, y);
//    }
//    
//    public void moveTo(double x, double y) {
//        this.physicalBody.translateToOrigin();
//        this.move(x, y);  
//    }
    
    protected Body getPhysicalBody() {
        return this.physicalBody;
    }

    public double getRadius() {
        return ((Circle) this.physicalBody.getFixture(0).getShape()).getRadius();
    }
    
    public Pair<Double, Double> getPosition() {
//        return this.position;
        Vector2 centre = this.physicalBody.getWorldCenter();
        return Pair.of(centre.x, centre.y);
    }
    
}
