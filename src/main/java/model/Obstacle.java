package model;

import org.apache.commons.lang3.tuple.Pair;
import org.dyn4j.dynamics.*;
import org.dyn4j.geometry.*;

public class Obstacle {

    private Body physicalBody;
    
    public Obstacle(double width, double height, double angle, Pair<Double, Double> position) {
        
        Rectangle shape = new Rectangle(width, height);
        shape.rotate(Math.toRadians(angle));
        shape.translate(shape.getCenter().getNegative());
        shape.translate(position.getLeft(), position.getRight());
        
        BodyFixture fixture = new BodyFixture(shape);
        
        this.physicalBody = new Body();
        this.physicalBody.addFixture(fixture);
        this.physicalBody.setMass(MassType.INFINITE); //makes it stand still
    }
    
    protected void update() {
        
    }
    
    protected Body getPhysicalBody() {
        return this.physicalBody;
    }
    
    public double getWidth() {
        return ((Rectangle) this.physicalBody.getFixture(0).getShape()).getWidth();
    }
    
    public double getHeight() {
        return ((Rectangle) this.physicalBody.getFixture(0).getShape()).getHeight();
    }
    
    public double getAngle() {
        return Math.toDegrees(((Rectangle) this.physicalBody.getFixture(0).getShape()).getRotationAngle());
    }
    
    public Pair<Double, Double> getPosition() {
        Vector2 centre = this.physicalBody.getWorldCenter();
        return Pair.of(centre.x, centre.y);
    }
    
}
